Jellyfin-Helm
=============

Open-Source media server (`Jellyfin <https://jellyfin.org/>`_) for `Kubernetes <https://kubernetes.io/>`_ clusters via `Helm <https://helm.sh/>`_ chart.

Wizard
------

Jellyfin by default has a quick-start wizard.
Since this wizard is enabled by default, the ingress of traffic shall not be on purpose.
To expose the wizard for first time setup:

.. code-block::

  kubectl port-forward -n jellyfin svc/jellyfin-service 8096:80

Replacing `-n jellyfin` with whatever namespace you have installed jellyfin to.
You can then go to 127.0.0.1:8096 to access the wizard. If it does not automagically redirect you, you can manually try calling the wizard via a url in your browser:

.. code-block::

  http://localhost:8096/web/index.html#!/wizardstart.html

Once the wizard has been configured and you have stored the config for reuse, you can then safeley expose the service with ingress.
